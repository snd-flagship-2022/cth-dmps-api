# cth-dmps-api

API web service for searching and retrieving DMPs from Chalmers University of Technology in RDA Common mdaDMP format. 

Before getting started, make sure you have installed [Node.js](https://nodejs.org/en/) and [Visual Studio Code](https://code.visualstudio.com/).

1. Clone this repository.
2. Run *npm install* from inside the directory.
3. Make a copy of *.env_example*.
4. Change the name of the copied file to *.env*
5. Add configurations to the *.env* file (you can try the project without adding anything).
6. Open the folder in *Visual Studio Code*.
7. Open *app.js* and click *f5* to run the project.
8. Go to localhost:3000 in your browser.

## Link to web site
todo